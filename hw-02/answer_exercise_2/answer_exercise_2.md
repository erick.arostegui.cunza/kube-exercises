#### - Crear un objeto de tipo replicaSet a partir del objeto anterior con las siguientes especificaciones:

    - Debe tener 3 replicas

answer_exercise_2.yml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02
---
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server    
spec:  
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:      
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80
```

Para crear el replica ejecutar el comando 

```shell
kubectl apply -f .\answer_exercise_2.yml
```

Al finalizar el ejercicio ejecutar el comando 

```shell
kubectl delete -f .\answer_exercise_2.yml
```

Resultado :

![](./images/01.jpg)

#### - ¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10?

Crear un objeto hpa (Horizontal Pod Autoscaler) con le siguiente comando

```shell
kubectl autoscale rs <name_rs> --min=10 --max=10 -n=<namespace>
```

```shell
kubectl autoscale rs nginx-server --min=10 --max=10 -n=hw-02
```

Resultado :

![](./images/02.jpg)

*En caso el hpa exista eliminar con el siguiente comando

```shell
kubectl delete hpa <name_rs> -n=<namespace>
```

```shell
kubectl delete hpa nginx-server -n=hw-02
```

#### - Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto)

Se debe agregar la especificacion "topology" con la llave "kubernetes.io/hostname"

answer_exercise_2.yml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02
---
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server    
spec:  
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: kubernetes.io/hostname
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app: nginx-server
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80
```

Para actualizar el replica ejecutar el comando 

```shell
kubectl apply -f .\answer_exercise_2.yml
```