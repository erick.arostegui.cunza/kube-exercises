#### - Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis utilizar la imagen del ejercicio 1. 

Recordad lo que hemos visto en clase sobre “Blue Green deployment”:

a. Existe una aplicación que está desplegada en el clúster (en el ejemplo, 1.0v):

![](./images/01.jpg)

b. Antes de ofrecer el servicio a los usuarios, la compañía necesita realizar una serie de validaciones con la versión 2.0. Los usuarios siguen accediendo a la versión 1.0:

![](./images/02.jpg)

c. Una vez que el equipo ha validado la aplicación, se realiza un switch del tráfico a
la versión 2.0 sin impacto para los usuarios:

![](./images/03.jpg)

## Resolucion

### 1.- Generar 2 imagenes docker y subirlas en un registry

&nbsp;&nbsp;&nbsp;&nbsp;1.1.- Version 1.0.0 a la aplicación hello world

&nbsp;&nbsp;&nbsp;&nbsp;Compilar la la imagen docker
```shell
docker build -t <repo_name>/<tag_name>:<tag_version> <context>
```
```shell
docker build -t scorpius8/hello-world:1.0.0 .\docker\1.0.0\
```

&nbsp;&nbsp;&nbsp;&nbsp;Subir la image a un registry
```shell
docker push <repo_name>/<tag_name>:<tag_version>
```
```shell
docker push scorpius8/hello-world:1.0.0
```

&nbsp;&nbsp;&nbsp;&nbsp;Probar la imagen
```shell
docker run --rm -it -p <host_port>:<container_port> <repo_name>/<tag_name>:<tag_version>
```
```shell
docker run --rm -it -p 8085:80 scorpius8/hello-world:1.0.0
```

![](./images/04.jpg)

![](./images/05.jpg)

&nbsp;&nbsp;&nbsp;&nbsp;1.2.- Version 2.0.0 a la aplicación hello world

&nbsp;&nbsp;&nbsp;&nbsp;Compilar la la imagen docker
```shell
docker build -t <repo_name>/<tag_name>:<tag_version> <context>
```
```shell
docker build -t scorpius8/hello-world:2.0.0 .\docker\2.0.0\
```

&nbsp;&nbsp;&nbsp;&nbsp;Subir la image a un registry
```shell
docker push <repo_name>/<tag_name>:<tag_version>
```
```shell
docker push scorpius8/hello-world:2.0.0
```

&nbsp;&nbsp;&nbsp;&nbsp;Probar la imagen
```shell
docker run --rm -it -p <host_port>:<container_port> <repo_name>/<tag_name>:<tag_version>
```
```shell
docker run --rm -it -p 8085:80 scorpius8/hello-world:2.0.0
```

![](./images/06.jpg)

![](./images/07.jpg)

### 2.- Generar un deployment para la versón 1.0.0 de la aplicación (BLUE)  
<br/>

Creando el namespace de trabajo

<br/>

namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02
```

Comando de ejecución
```shell
kubectl apply -f namespace.yaml
```
<br/>

Creando el deployment BLUE

<br/>

blue-deploy.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: blue-deployment
  namespace: hw-02
spec:
  selector:
    matchLabels:
      app: blue-deployment
      version: hello-world-1.0.0
  replicas: 3
  template:
    metadata:
      labels:
        app: blue-deployment
        version: hello-world-1.0.0
    spec:
      containers:
        - name: blue-deployment          
          image: scorpius8/hello-world:1.0.0
```

Comando de ejecución
```shell
kubectl apply -f blue-deploy.yaml
```

<br/>

Resultado

<br/>

![](./images/08.jpg)

<br/>

### 3.- Generar un servicio que exponga el Deployment BLUE y GREEN (uno a la vez, es decir, primero expondra el deployment BLUE y luego este sera actualizado para exponer el deployment GREEN)
<br/>

Creando el servicio BLUE-GREEN

<br/>

blue-service.yaml
```yaml
apiVersion: v1
kind: Service
metadata: 
  name: blue-green-service
  namespace: hw-02
  labels: 
    app: blue-deployment
    version: hello-world-1.0.0
spec:
  type: LoadBalancer
  ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: 80
  selector: 
    app: blue-deployment
    version: hello-world-1.0.0  
```

Comando de ejecución
```shell
kubectl apply -f blue-service.yaml
```

<br/>

Probando el servicio con minikube

<br/>

Comando de ejecución
```shell
minikube service --url blue-green-service -n=hw-02
```

<br/>

Resultado

<br/>

![](./images/09.jpg)

![](./images/10.jpg)

<br/>

### 4.- Generar un deployment para la versón 2.0.0 de la aplicación (GREEN)  
<br/>

Creando el deployment GREEN

<br/>

green-deploy.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: green-deployment
  namespace: hw-02
spec:
  selector:
    matchLabels:
      app: green-deployment
      version: hello-world-2.0.0
  replicas: 3
  template:
    metadata:
      labels:
        app: green-deployment
        version: hello-world-2.0.0
    spec:
      containers:
        - name: green-deployment
          image: scorpius8/hello-world:2.0.0
```

Comando de ejecución
```shell
kubectl apply -f green-deploy.yaml
```

<br/>

Resultado

<br/>

![](./images/11.jpg)

<br/>

### 5.- Actualizar el servicio BLUE-GREEN para que exponga el deployment GREEN
<br/>

Actualizando el servicio BLUE-GREEN

<br/>

green-service.yaml
```yaml
apiVersion: v1
kind: Service
metadata: 
  name: blue-green-service
  namespace: hw-02
  labels: 
    app: green-deployment
    version: hello-world-2.0.0
spec:
  type: LoadBalancer
  ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: 80
  selector: 
    app: green-deployment
    version: hello-world-2.0.0   
```

Comando de ejecución
```shell
kubectl apply -f green-service.yaml
```

<br/>

Resultado

<br/>

![](./images/12.jpg)

![](./images/13.jpg)

(*) La actualizacion puede demorar varios segundos hasta que se aplique el cambio.

<br/>

### 6.- Limpiando el entorno de trabajo
<br/>

Despues de ejecutar el ejercicio, debe eliminar los objetos creados

```shell
kubectl delete -f blue-service.yaml
kubectl delete -f blue-deploy.yaml
kubectl delete -f green-deploy.yaml
kubectl delete -f namespace.yaml
```