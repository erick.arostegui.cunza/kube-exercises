#### - Crea un pod de forma declarativa con las siguientes especificaciones:
    - Imagen: nginx
    - Version: 1.19.4
    - Label: app: nginx-server
    - Limits
        CPU: 100 milicores
        Memoria: 256Mi
    - Requests
        CPU: 100 milicores
        Memoria: 256Mi

answer_exercise_1.yml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02

---
apiVersion: v1
kind: Pod
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:
  containers:
  - name: nginx-server
    image: nginx:1.19.4
    resources:
      limits:
        cpu: "100m"
        memory: "256Mi"
      requests:
        cpu: "100m"
        memory: "256Mi"
    ports:
    - containerPort: 80 
```

Para crear el pod ejecutar el comando 

```shell
kubectl apply -f .\answer_exercise_1.yml
```

Al finalizar el ejercicio ejecutar el comando 

```shell
kubectl delete -f .\answer_exercise_1.yml
```

#### - ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?

Se debe ejecutar el siguiente comando

```shell
kubectl logs <name_pod> <name_container> -n <namespace> --tail=10
```

```shell
kubectl logs nginx-server nginx-server -n hw-02 --tail=10
```

Resultado :

![](./images/01.jpg)

#### - ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías.

Para la IP del pod a nivel del Cluster usaria el comando

```shell
 kubectl get pod <pod_name> -o wide -n=<namespace>
 ```

```shell
 kubectl get pod nginx-server -o wide -n=hw-02
 ```

Resultado :

![](./images/02.jpg)

#### - ¿Qué comando utilizarías para entrar dentro del pod?

utilizaria el siguiente comando

```shell
 kubectl exec --stdin --tty <name_pod> -n=<namespace> -- /bin/bash
 ```

```shell
 kubectl exec --stdin --tty nginx-server -n=hw-02 -- /bin/bash
 ```

 Resultado :

 ![](./images/03.jpg)

 #### - Necesitas visualizar el contenido que expone NGINX, ¿qué acciones debes llevar a cabo?

 Ejecutaria el siguiente comando

```shell
 kubectl port-forward <name:pod> <port_host>:<port_pod> -n=<namespace>
 ```

```shell
 kubectl port-forward nginx-server 8085:80 -n=hw-02
 ```

Resultado :

 ![](./images/04.jpg)

 ![](./images/05.jpg)

 #### - Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado?

 Para obtener el detalle del pod ejecutar el comando

```shell
 kubectl describe pod <name_pod> -n=<namespace>
 ```

```shell
 kubectl describe pod nginx-server -n=hw-02
 ```

Resultado :

 ![](./images/06.jpg)