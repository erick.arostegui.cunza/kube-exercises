#### - Crear un objeto de tipo deployment con las especificaciones del ejercicio 1.

answer_exercise_4.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:
  selector:
    matchLabels:
      app: nginx-server
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.5
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:  
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
```

Para crear el deployment y el servicio ejecutar el comando 

```shell
kubectl apply -f .\answer_exercise_4.yaml
```

Al terminar el ejercicio ejecutar el comando 

```shell
kubectl delete -f .\answer_exercise_4.yaml
```

Resultado :

![](./images/01.jpg)

#### - Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”

answer_exercise_4.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:  
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
```

Para actualizar el deployment y el servicio ejecutar el comando 

```shell
kubectl apply -f .\answer_exercise_4.yaml
```

Resultado :

![](./images/02.jpg)

#### - Despliega una nueva versión haciendo “rollout deployment”
 
 answer_exercise_4.yaml
 ```yaml
 apiVersion: v1
kind: Namespace
metadata:
  name: hw-02

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.6
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:  
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
 ```

Para actualizar el deployment y el servicio ejecutar el comando 

```shell
kubectl apply -f .\answer_exercise_4.yaml
```

```shell
kubectl rollout status deployment/<name_deployment> -n=<namespace>
```

```shell
kubectl rollout status deployment/nginx-server -n=hw-02
```

Resultado :

![](./images/03.jpg)

#### - Realiza un rollback a la versión generada previamente

para revisar las versiones utilizar el comando 

```shell
kubectl rollout history deployment/<name_deployment> -n=<namespace>
```

```shell
kubectl rollout history deployment/nginx-server -n=hw-02
```

para realizar un rollback a la version anterior utilizar el comando 

```shell
kubectl rollout undo deployment/<name_deployment> -n=<namespace>
```

```shell
kubectl rollout undo deployment/nginx-server -n=hw-02
```

Resultado :

![](./images/04.jpg)