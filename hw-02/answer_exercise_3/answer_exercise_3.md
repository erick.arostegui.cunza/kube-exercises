#### - Crea un objeto de tipo service para exponer la aplicación del ejercicio anterior de las siguientes formas:

- Exponiendo el servicio hacia el exterior (crea service1.yaml)
   
service1.yml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02
---
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server    
spec:  
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: kubernetes.io/hostname
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app: nginx-server
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:
  type: LoadBalancer #Exponiendo el servicio hacia el exterior
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para crear el replica y el servicio ejecutar el comando 

```shell
kubectl apply -f .\service1.yml
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Si se usa minikube ejecutar el siguiente comando para exponer en servicio en el host

```shell
 minikube service --url <name_svc> -n=<namespace>
```

```shell
 minikube service --url nginx-server -n=hw-02
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Luego ejecutar el comando 

```shell
kubectl delete -f .\service1.yml
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resultado :

![](./images/01.jpg)

![](./images/02.jpg)

- De forma interna, sin acceso desde el exterior (crea service2.yaml)

service2.yml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02
---
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server    
spec:  
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: kubernetes.io/hostname
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app: nginx-server
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:
  type: ClusterIP #De forma interna, sin acceso desde el exterior
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para crear el replica y el servicio ejecutar el comando 

```shell
kubectl apply -f .\service2.yml
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Si se usa minikube ejecutar el siguiente comando para exponer en servicio en el host

```shell
 minikube service --url <name_svc> -n=<namespace>
```

```shell
 minikube service --url nginx-server -n=hw-02
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Luego ejecutar el comando 

```shell
kubectl delete -f .\service2.yml
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resultado :

![](./images/03.jpg)

![](./images/04.jpg)

- Abriendo un puerto especifico de la VM (crea service3.yaml)

service3.yml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-02
---
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server    
spec:  
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: kubernetes.io/hostname
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app: nginx-server
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "100m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "256Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-02
  labels:
    app: nginx-server
spec:
  type: NodePort #Abriendo un puerto especifico de la VM
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    nodePort: 31234
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para crear el replica y el servicio ejecutar el comando 

```shell
kubectl apply -f .\service3.yml
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Si se usa minikube ejecutar el siguiente comando para exponer en servicio en el host

```shell
 minikube service --url <name_svc> -n=<namespace>
```

```shell
 minikube service --url nginx-server -n=hw-02
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Luego ejecutar el comando 

```shell
kubectl delete -f .\service3.yml
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resultado :

![](./images/05.jpg)

![](./images/06.jpg)
