### 2. [StatefulSet] Crear un StatefulSet con 3 instancias de MongoDB (ejemplo visto en clase):

<br/>

Se pide :

<ul>
    <li>Habilitar el clúster de MongoDB</li>
    <li>Realizar una operación en una de las instancias a nivel de configuración y verificar que el cambio se ha aplicado al resto de instancias</li>
    <li>Diferencias que existiría si el montaje se hubiera realizado con el objeto de ReplicaSet</li>        
</ul>
<br/>

#### Resolucion

<br/>

Crear un namespace hw-03

mongodb-namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-03
 ```

Comando a ejecutar para generar el namespace
```shell
kubectl apply -f .\mongodb-namespace.yaml
```

![](./images/01.jpg)

<br/>

Crear un servicio headless

mongodb-service.yaml
```yaml
apiVersion: v1
kind: Service
metadata:
  name: mongodb-svc
  namespace: hw-03
  labels:
    app: db
    name: mongodb
spec:
  clusterIP: None
  selector:
    app: db
    name: mongodb
  ports:
  - port: 27017
    targetPort: 27017  
 ```

Comando a ejecutar para generar el servicio
```shell
kubectl apply -f .\mongodb-service.yaml
```

Comando para revisar el servicio
```shell
kubectl get svc -n hw-03
```

Comando para revisar el el endpoint
```shell
kubectl describe ep mongodb-svc -n hw-03
```

![](./images/02.jpg)

<br/>

Crear el recurso StatefulSet

mongodb-statefulset.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongo-express
  namespace: hw-03
  labels:
    app: web
    name: mongo-express
spec:
  replicas: 2
  selector:
    matchLabels:
      app: web
      name: mongo-express
  template:
    metadata:
      labels:
        app: web
        name: mongo-express
    spec:
      containers:
      - name: mongo-express
        image: mongo-express:latest
        env:
        - name: ME_CONFIG_MONGODB_SERVER
          value: mongo-0.mongodb-svc.hw-03.svc.cluster.local,mongo-1.mongodb-svc.hw-03.svc.cluster.local,mongo-2.mongodb-svc.hw-03.svc.cluster.local?replicaSet=rs0
          #value:  mongo-0.mongodb-svc,mongo-1.mongodb-svc,mongo-2.mongodb-svc?replicaSet=rs0
        - name: ME_CONFIG_MONGODB_ENABLE_ADMIN
          value: "true"
        - name: ME_CONFIG_BASICAUTH_USERNAME
          value: "admin-username"
        - name: ME_CONFIG_BASICAUTH_PASSWORD
          value: "admin-password"        
        ports:
          - containerPort: 8081

---

apiVersion: v1
kind: Service
metadata:
  name: mongo-express-svc
  namespace: hw-03
spec:
  type: NodePort
  selector:
    app: web
    name: mongo-express
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8081
```

Comando a ejecutar para generar el statefulset
```shell
kubectl apply -f .\mongodb-statefulset.yaml
```

Comando para revisar el statefulset
```shell
kubectl get statefulset -n hw-03
```

Comando para revisar los pods
```shell
kubectl get pods -n hw-03
```

Comando para revisar el endpoint
```shell
kubectl describe endpoints mongodb-svc -n hw-03
```

![](./images/03.jpg)

<br/>

Inicializar el replica set de MongoDB

Comando para ingresar al nodo master
```shell
kubectl exec -it mongo-0 mongo -n hw-03
```

Dentro del nodo ejecutar
```shell
rs.initiate({_id: "rs0", version: 1, members: [
  { _id: 0, host : "mongo-0.mongodb-svc.hw-03.svc.cluster.local:27017" },
  { _id: 1, host : "mongo-1.mongodb-svc.hw-03.svc.cluster.local:27017" },
  { _id: 2, host : "mongo-2.mongodb-svc.hw-03.svc.cluster.local:27017" }
]});
```

![](./images/04.jpg)

Comando para validar configuracion
```shell
rs.conf()
```

![](./images/05.jpg)

<br/>

La diferencia entre un replicaset y statefulset es:

<ul>
  <li>Los nombres de los pods; replicaset genera segun el algoritmo de kubernetes; statefulset los asigna de manera secuencial comenzando de 0 </li>
  <li>La generacion de pods; replicaset elimina o agrega segun disponibilidad de kubernetes; statefulset elimina o agrega de forma secuencia </li>
</ul>