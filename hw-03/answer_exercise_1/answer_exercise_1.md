### 1. [Ingress Controller / Secrets] Crea los siguientes objetos de forma declarativa con las siguientes especificaciones:

<br/>
<ul>
    <li>Imagen: nginx</li>
    <li>Version: 1.19.4</li>
    <li>3 replicas</li>
    <li>Label: app: nginx-server</li>
    <li>Exponer el puerto 80 de los pods</li>
    <li>Limits:
        <ul>
            <li>CPU: 20 milicores</li>
            <li>Memoria: 128Mi</li>
        </ul>
    </li>   
    <li>Requests:
        <ul>
            <li>CPU: 20 milicores</li>
            <li>Memoria: 128Mi</li>
        </ul>
    </li>
</ul>
<br/>

Resolucion

deploy-service.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-03

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  namespace: hw-03
  labels:
    app: nginx-server
spec:
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "20m"
            memory: "128Mi"
          requests:
            cpu: "20m"
            memory: "128Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-03
  labels:
    app: nginx-server
spec:  
  type: ClusterIP
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
```

<br/>

Comando a ejecutar para desplegar
```shell
kubectl apply -f .\deploy-service.yaml
```

<br/>
<br/>

#### a.- A continuación, tras haber expuesto el servicio en el puerto 80, se deberá acceder a la página principal de Nginx a través de la siguiente URL: http://<student_name>.student.lasalle.com 

<br/>

Diagrama:
![](./images/01.jpg)

<br/>

Validación:
- Navegador web
![](./images/02.jpg)

- Otra forma, mediante la terminal a partir de la ejecución de la siguiente instrucción:

curl -k http://<student_name>.student.lasalle.com

![](./images/03.jpg)

<br/>

#### Resolución

<br/>

(*) En caso utilice minikube iniciar minikube con driver de Hyper-V o un virtualizadro de maquinas, ya que si lo hace con docker el acceso externo es complejo.

```shell
minikube start --driver=hyperv 
```

Activar ingress en minikube
```shell
minikube addons enable ingress
```

ingress.yaml
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-server-ingress
  namespace: hw-03
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  rules:
    - host: erick.student.lasalle.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: nginx-server
                port:
                  number: 80
```

Comando a ejecutar para desplegar
```shell
kubectl apply -f .\ingress.yaml
```

Comando para obtener el detalle del servicio
```shell
kubectl get svc -n=hw-03
```

Comando para obtener el ip de minikube
```shell
minikube ip
```

Asociar el ip con el hostname, ir a "C:\Windows\System32\drivers\etc", abrir el archivo "hosts" y agregar la relacion

![](./images/04.jpg)

Validacion por CURL
```shell
curl http://erick.student.lasalle.com/
```
![](./images/05.jpg)

Validacion por browser
```shell
curl http://erick.student.lasalle.com/
```
![](./images/06.jpg)

<br/>
<br/>

#### b. Una vez realizadas las pruebas con el protocolo HTTP, se pide acceder al servicio mediante la utilización del protocolo HTTPS, para ello:

<br/>

<ul>
    <li>Crear un certificado mediante la herramienta OpenSSL u otra similar</li>
    <li>Crear un secret que contenga el certificado</li>
</ul>

<br/>

Validación:

- Navegador web

Como el certificado no está expedido por una entidad de confianza (Comodo, DigiCert,Let’s Encrypt, … ), os aparecerá el siguiente mensaje en el navegador:

![](./images/07.jpg)

Deberéis hacer click en Mostrar detalles > Visitar este sitio web:

![](./images/08.jpg)

Si lo habéis configurado correctamente, accederéis a la página principal de Nginx. Si os fijáis en la barra superior aparecerá un candado, eso significa que estáis accediendo mediante HTTPS. Si hacéis click en el certificado y lo mostráis, deberéis ver el certificado creado:

![](./images/09.jpg)

- Otra forma, mediante terminal a partir del siguiente comando curl:
<br/>

curl --cacert <certificate> https://<student_name>.student.lasalle.com

![](./images/10.jpg)

Aclaraciones:
- En la URL, se deberá sustituir <student_name> por el nombre del estudiante
- No es necesario cambiar los ficheros de configuración de Nginx
- No es necesario exponer el servicio hacia el exterior (ClusterIP)

<br/>

#### Resolución

<br/>

Install openssl (windows)
```shell
choco install openssl
```
<br/>

Generar una llave publica (cert) y una privada (key)

- ingress-tls.crt
- ingress-tls.key
```shell
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${KEY_FILE} -out ${CERT_FILE} -subj "/CN=${DOMAIN}/O=${COMPANY}"
```

```shell
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ingress-tls.key -out ingress-tls.crt -subj "/CN=erick.student.lasalle.com/O=ingress-tls"
```
<br/>

Crear secreto

- Encodificar las llaves (crt y key) a base 64, puede utilizar esta URL "https://www.base64encode.net/"

![](./images/11.jpg)

secret.yaml
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: ingress-tls
  namespace: hw-03
data:
  tls.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlEVlRDQ0FqMmdBd0lCQWdJVVAxTFJXS0xDWVZuSk93eDVEazAzQzBpOUZXb3dEUVlKS29aSWh2Y05BUUVMDQpCUUF3T2pFaU1DQUdBMVVFQXd3WlpYSnBZMnN1YzNSMVpHVnVkQzVzWVhOaGJHeGxMbU52YlRFVU1CSUdBMVVFDQpDZ3dMYVc1bmNtVnpjeTEwYkhNd0hoY05NakV4TVRJeU1qRTFORFV5V2hjTk1qSXhNVEl5TWpFMU5EVXlXakE2DQpNU0l3SUFZRFZRUUREQmxsY21samF5NXpkSFZrWlc1MExteGhjMkZzYkdVdVkyOXRNUlF3RWdZRFZRUUtEQXRwDQpibWR5WlhOekxYUnNjekNDQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFMcTdWQlRkDQp6Z1IxdEMvbE10WjZYQkVJaWlXNzRuZ0JKemREa081Mm82cmpYMFFwTmJURVZaV21XTkZMSGxlWHIvamg4RXZHDQphcWhVOGs0aE93TTQ1MXc4OVZnZ1ErWkxXTDV5Mi9KSmNsUkNKMStuUVRTN0kyZUd4cVp4WUROOEdvSzlRTTZFDQpsYm1FT3ViOW5XMlVQL0ZLbUEraWwyRlVlckhhTEp0c3JSWHpOTU9Ucmd3dDNWSXc0OXFOZVU5VEY3ZUpPY1AxDQpxMzN3ZElOdHZ4ZTBiV1k5c2NFUkRCaGhZNTRFYVd2VGhnQlNVZTFGTkttWUN5aVdFM3FrbXhFOGdrV0lzSDU3DQpVUEJCcllmTnFmVUlZRXZJb3NWT2Y1ZHdCcG5lWGk3QXBxaHhIUHZSQ0doekQxYWxtZHl0TDlsZDNMdUhNblJvDQpqMVU4cDdGNGF1dGpHUk1DQXdFQUFhTlRNRkV3SFFZRFZSME9CQllFRkFIbXBoOExibE1WalVrR2xPazlGbTRnDQpLbjdWTUI4R0ExVWRJd1FZTUJhQUZBSG1waDhMYmxNVmpVa0dsT2s5Rm00Z0tuN1ZNQThHQTFVZEV3RUIvd1FGDQpNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFKdTVoTE9wVDA3alM0UE5ZVVJ3UE8reG43bHN4emtnDQo4M0lIcC9BQk16dW9uY0VTK2pESktxektRMUtQN3FYQzhRcDUvZ3pKNVpMcFAyVko3TzUxbmFHQlc3ZUhLS2dDDQpaUDB4NnBhbXFwR2Y2NTZXZjR5R3JJMHMveDFqa0l0SFZvZmREVVRoMmhKMTl0QzhxZkljbW9ielVIU2U0TThBDQpDUjBaWEZGZlNPckYwRWx1QUdyWGFvdG1NaWdIejk5MUwzRXlOSnZ2RkdwMmpsU0ZuS3kzZTQ1UG1NMXJrSHFtDQppTWNOU2RQK2NOUmxMMnBSMENBOXZzNXFpRnhKV0lTa295WW1tR0JKZk1hai96OXpCbFZwWHFBeEdVWjZjTW1jDQpzeC85c05HUGJKcjR2OTNTdmNUNXdhd2NGemNSWnJjL1EvWDcwSHE5amsrcUwxbHU3NUQ0d1g0PQ0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ0K
  tls.key: LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tDQpNSUlFdlFJQkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0JLY3dnZ1NqQWdFQUFvSUJBUUM2dTFRVTNjNEVkYlF2DQo1VExXZWx3UkNJb2x1K0o0QVNjM1E1RHVkcU9xNDE5RUtUVzB4RldWcGxqUlN4NVhsNi80NGZCTHhtcW9WUEpPDQpJVHNET09kY1BQVllJRVBtUzFpK2N0dnlTWEpVUWlkZnAwRTB1eU5uaHNhbWNXQXpmQnFDdlVET2hKVzVoRHJtDQovWjF0bEQveFNwZ1BvcGRoVkhxeDJpeWJiSzBWOHpURGs2NE1MZDFTTU9QYWpYbFBVeGUzaVRuRDlhdDk4SFNEDQpiYjhYdEcxbVBiSEJFUXdZWVdPZUJHbHIwNFlBVWxIdFJUU3BtQXNvbGhONnBKc1JQSUpGaUxCK2UxRHdRYTJIDQp6YW4xQ0dCTHlLTEZUbitYY0FhWjNsNHV3S2FvY1J6NzBRaG9jdzlXcFpuY3JTL1pYZHk3aHpKMGFJOVZQS2V4DQplR3JyWXhrVEFnTUJBQUVDZ2dFQWYrVUVQdEo0WTVlV2xXTmZRWVZHLzZlZ3J4MlRqR3dGeHhVdzlCTXdJRUJoDQpKSmN4SUtHRW1OMzVZVWdZL1JkNHdIZEhsUzdPS0lNZC9xTkYvdnYvZ3E2QndCMUJSRCtCQmRnM2l5Z0E5WExsDQo4dDdraTFCTmZIL1VHWFgzMU91QVlPRnN2TVlVb3A3TmxSNUpzQjdrK01nRmhGZnhaUWx1aTlnSnkxNVE0b0dvDQpmWmswdnZPYVFNMmQxK21DbTlsRjdNV3NSUjBQZURoTm1MWDJkT3IrQkJtWkZvR0JDbjVFdEtwUW9iM0k4RzVyDQpRbVo2R1hmRHlxWFV6cG11SUt6VmEyNlp1eWU0VTFTbDZ0dmJUOHZ6R1RKTE0weHFjaVAzTzRicWFsbkI3UWJ5DQpUa0FQemk5T2dvK1cvNmtwb0tMdDRBZFF6dTBtd0JPbkN5blBzb283eVFLQmdRRGpPVWU0T0Z0aXJUZW1oQ3ZVDQoyWWNrZHJheGNiNmVkaTYzUG81QkFzcmRMUEcvRDhXVjBzR0RhN09ObWhqak9vM1NRS3BTUDBOVDllM0Q5U1hyDQp1RzQrUDlpYzdMQTRqdWhWdnl4NGpFOEI3VDBnd0NUSEZaNmFSNU4yOTJyVWpXUVg1dzFDWkhoeHBTODJlOUluDQpVK01jRVZyS2RLU2VZMGhFUytZQXhKV3NCd0tCZ1FEU1lVWjNVdGtKWHVNNHRZZ2hZdzBINUpORVBNbEVOL3FPDQpSa1hYWTVHSGJGa3F2N0tjNDIrd0xuMG5ZQVFjczBJMGQ0K2JPUFdCTnZMTU05eXVYU25IWk9jY2lzT3drZlVsDQp3Z204MGdEOWVnVlNEQTM4eTA2cDJ6VnVVV2RUK0EvNDR2Tm9DV0VYejZTdGp2NWYyeFpUMm5xRFBIbnV0c0RHDQpCZitVb2YzL2xRS0JnUUNzZVlRNUQzNCtqcUVaWW1ZcThOc21FSDVvZnFCa2creTk1MXZ2ZVFFbmxjQUNlU1JQDQpyMlI0THo1aGs4Q2thb1BRUG9tS1dMN3djbEtDVmdqc3ZsZmdKSEoxMnl1QUl1bTBmS0p5OW4zYVJvd0oyZytRDQpscGFISVNHTVFnM3pQVWZtNU5rcHRqMitkZFk2a3c1WUQ5THFNVG9yUnhMVnJ5TGh4SUxYdm1DbDdRS0JnQW9LDQppb3AzY2RSNHF1K3grbVZkak9tcmh6dmlpdGl1czA3SEc5NUQ2bFpFWFowa3VTbXowVWpmYnluSjgyaExSZ1dyDQpEemVoYUZaYWdRd3VEWUprLy9GOFoydDlWQW5CYmFuM3NPOUtOckhpZVE2SmRNRjBFclVZdVZ3aWxvUHpldyt1DQpCV0FaMG9DSWNKdTREM3dUd09VUHNIbERFYTdMR2poWllQRi9LZGhoQW9HQUlvSyt1T2N6TC9wdVRXRGRPN1hSDQptbDJtem9nVFRjbHpkV0hUUlBaOVd0V1puWlh4RndqdFRGZXluSnAzZmpQNlIrSWgvYnI4Ung5TGI5ZnVjbE9aDQpGS3NWd2JuNnpEQ1hWRHdCaVpyUjhtQmx2WUE0SUt1cFJCNjNzM0VGcW9aZXFuNHh6bzN4cTFrSXYrcHdkQVJhDQo2MTJRQ3FyNTA5U01WejMvMUxManRFOD0NCi0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0NCg==
type: kubernetes.io/tls
```

Comando a ejecutar para crear secret
```shell
kubectl apply -f .\secret.yaml
```
<br/>

Actualizar ingress, agregando configuracion de TLS

ingress.yaml
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-server-ingress
  namespace: hw-03
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  tls:
  - hosts:
    -  erick.student.lasalle.com
    secretName: ingress-tls
  rules:
    - host: erick.student.lasalle.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: nginx-server
                port:
                  number: 80

```

Comando a ejecutar para actualizar ingress
```shell
kubectl apply -f .\ingress.yaml
```
<br/>

Resultado :

<br/>

Ejecucion de comandos

![](./images/12.jpg)

Validación por browser
<br/>

Indica no seguro porque se usa un certificado autofirmado

![](./images/13.jpg)


Click en el boton "No seguro"

![](./images/14.jpg)


Seleccionar la opcion "certificado no es válido"

![](./images/15.jpg)

Se aprecia el dominio y la fecha de caducidad.

![](./images/16.jpg)

En el detalle de ve los datos de la organización y el dominio

![](./images/17.jpg)
<br/>

Validaciónen con un CURL
<br/>

curl -v -k https://erick.student.lasalle.com

![](./images/19.jpg)

<br/>
<br/>

Luego de terminar el ejercicio ejecutar los siguientes comandos: 
<br/>

```shell
 kubectl delete -f .\ingress.yaml
 kubectl delete -f .\secret.yaml
 kubectl delete -f .\deploy-service.yaml
 ```