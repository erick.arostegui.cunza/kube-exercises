### 3. [Horizontal Pod Autoscaler] Crea un objeto de kubernetes HPA, que escale a partir de las métricas CPU o memoria (a vuestra elección). Establece el umbral al 50% de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas.

<br/>

Podéis realizar una prueba de estrés realizando un número de peticiones masivas mediante la siguiente instrucción:

```shell
kubectl -n hw-03 run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://nginx-server; done"
```

<br/>

#### Resolucion

<br/>

Crear un namespace hw-03

namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hw-03
```

Comando a ejecutar para generar el namespace
```shell
kubectl apply -f .\namespace.yaml
```

![](./images/01.jpg)

<br/>

Crear un deploy y un servicio "ginx-server"

deploy-service.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  namespace: hw-03
  labels:
    app: nginx-server
spec:
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  replicas: 1
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.4
        resources:
          limits:
            cpu: "20m"
            memory: "128Mi"
          requests:
            cpu: "20m"
            memory: "128Mi"
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  namespace: hw-03
  labels:
    app: nginx-server
spec:  
  type: ClusterIP
  ports:
  - port: 80 #Pod Port
    targetPort: 80 #Container Port
    protocol: TCP
    name: http  
  selector:
    app: nginx-server
```

Comando a ejecutar para generar el deploy y servicio
```shell
kubectl apply -f .\deploy-service.yaml
```

![](./images/02.jpg)

<br/>

Crear el recurso HPA

hpa.yaml
```yaml
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-server
  namespace: hw-03
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-server
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50
  - type: Resource
    resource:
      name: memory 
      target:
        type: Utilization 
        averageUtilization: 50
```

Comando a ejecutar para generar el hpa
```shell
kubectl apply -f .\hpa.yaml
```

![](./images/03.jpg)

<br/>

En otra consola ejecutar
```shell
kubectl -n hw-03 run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://nginx-server; done"
```

![](./images/04.jpg)

Revisando el HPA

![](./images/05.jpg)

Luego de unos minutos

![](./images/06.jpg)